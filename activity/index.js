let http = require("http");

const server = http
  .createServer((request, response) => {
    response.writeHead(200, { "Content-Type": "text/plain" });
    if (request.url == "/") {
      response.end("this is home");
    } else if (request.url == "/login") {
      response.end("please enter credentials");
    } else {
      response.end("sorry the page cannot be found");
    }
  })
  .listen(5000);

console.log("mors");
