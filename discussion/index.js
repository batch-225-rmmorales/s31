// node js introduction
// let http = require("http"); old syntax im gonna use ES6

// Use the "require" directive load Node.js modules
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol(HTTP)
// HTTP is a protocol that allows the fetching of resources such as HTML documents.
// The message sent by the "client", usually a Web browser, called "request"
// The messahe sent by the "server" as an answers are called "responses"
let http = require("http");
//import * as http from "http";
// http
//   .createServer(function (request, response) {
//     response.writeHead(200, { "Content-Type": "text/plain" });
//     response.end("Welcome to my page");
//   })
//   .listen(5000);

// console.log("server is running");

const server = http
  .createServer((request, response) => {
    response.writeHead(200, { "Content-Type": "text/plain" });
    if (request.url == "/") {
      response.end("this is home");
    } else if (request.url == "/greetings") {
      response.end("this is greetings");
    } else {
      response.end("error 404 page not found");
    }
  })
  .listen(5000);

console.log("mors");
